const readline = require("readline");

const daysOfWeek = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday"];
const subjects = ["Mathematics", "English", "Kiswahili", "Science", "Social Studies"]


console.log("Welcome to the timetable app");
console.log("Enter a page number between 1 to 5 as Day of the week to view timetable of the day")
const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

rl.question('Enter page number? ', (answer) => {
    if (Number(answer) === 'NaN') {
        console.error("Invalid input");
    } else {
        if (Number(answer) < 1 && Number(answer) > 5) {
            console.error("Invalid day of week")
        } else {
            displayTimetable(answer)
        }

    }
    rl.close();
});

const displayTimetable = (number) => {
    console.log(`${daysOfWeek[number - 1]}: ${subjects[number - 1]}`);
}